const express = require('express');
const bodyParser = require('body-parser');
const db = require('./models');

const port = 3000;
const app = express();
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.send('Hello World!')
});

app.get('/todos', async (req, res, next) => {
    try {
        const todos = await db.Todo.findAll();
        res.json(todos);
    } catch (err) {
        next(err);
    }
});

app.post('/todos', async (req, res, next) => {
    try {
        const todo = await db.Todo.create({ text: req.body.text });
        res.json(todo);
    } catch (err) {
        next(err);
    }
});

app.listen(port, () => {
    console.log(`Todo app listening at http://localhost:${port}`)
});
