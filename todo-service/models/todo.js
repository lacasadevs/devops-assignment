'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Todo extends Model {
    static associate(models) {
      // define association here
    }
  };
  Todo.init({
    text: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Todo',
    tableName: 'todos'
  });
  return Todo;
};