# Todo service

Sample todo service using NodeJS and mysql database

> Note: This code doesn't follow the coding best practices. It is kept as basic as possible for testing devops engineering.

## Running locally

- Ensure you have [Docker](https://docs.docker.com/get-docker/) and [NodeJS](https://nodejs.org/en/download/) installed

- Start mysql
```sh
docker run -p3306:3306 --rm \
    -e MYSQL_RANDOM_ROOT_PASSWORD=true \
    -e MYSQL_DATABASE=todo_service_dev \
    -e MYSQL_USER=todo_service_dev \
    -e MYSQL_PASSWORD=todo_service_dev \
    mysql:5.7.25
```

- Run
```
cd todo-service
npm install
```

- Run migrations to setup the database
```sh
npm run db:migrate
```

- Run the service
```
node app.js
```

- Test your setup
```sh
curl -XPOST localhost:3000/todos -H "Content-Type: application/json" -d '{"text": "Complete the assignment!"}'
curl localhost:3000/todos
```

## Running in a production like environment

- Set the following environment variables

```sh
NODE_ENV=production

DB_HOSTNAME=<hostname>
DB_PORT=<port>
DB_NAME=<db-name>
DB_USERNAME=<user-name>
DB_PASSWORD=<password>
```

- Run the below command to setup database schema

```sh
npm run db:migrate
```

- Run the below command to start the service

```sh
node app.js
```
