# Devops assignment

This assignment is to understand the devops engineer's ability to automate and design the solution using best practices

## Part 1 (Basic): Automate the deployment of a service in kubernetes

Automate the deployment of [todo service](./todo-service) as docker container in kubernetes. Send us the `Dockerfile`, deployment code and instructions to test.

The instructions for running [todo service](./todo-service) in a production like environment is available in [README.md](./todo-service/README.md)

### Requirements

- Code should allow deploying one or more replicas of the service and access it using single endpoint
- Code should be organised well to allow deployment of the service in multiple environments (eg: staging, production)

### Assumptions

- You can push the [todo service](./todo-service) docker image to a public registry like [docker hub](https://hub.docker.com/) for testing
- You can use any opensource tool that can help you achieve the goal faster
- You can test it locally using minikube, kind, etc. Same deployment code should be usable in any k8s cluster (eg: AWS EKS)
- Assume mysql is running outside the k8s cluster (you can run mysql locally or ephemerally in k8s cluster for testing)

> Note: Assuming prerequisites(i.e docker, k8s) are installed & working, it takes around 3-4 hours to complete this part

Please do not push your code to a public repository like github to avoid plagiarism. Thank you!

## Part 2 (Bonus): Infrastructure architecture for containerised 3 tire application in AWS

> Note: This is an optional exercise

Provide infrastructure architecture for containerised 3 tire  application(web, api, database) in AWS. Assume stateless services are deployed in AWS EKS.

> Tip: You can use [draw.io / app.diagrams.net](https://app.diagrams.net/) for writing the diagram

### Requirements

- Service can be deployed in multiple environments (eg: staging, production)
- Follow the best practices for infrastructure setup

> Note: It takes around 1-2 hours to complete this part

## Came here by accident?

Apply [here](https://casaone.skillate.com/jobs?page=0&pageSize=10&department=&title=devops) to work with us as Devops engineer and send us your solution :)
